﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Hubs
{
    public class DrawHub :Hub
    {
        ApplicationDbContext _context;
        public DrawHub(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task GetData()
        {
            

            var dateTime =DateTime.Now.AddMinutes(30).AddMinutes(-15).AddHours(5);
            LotteryEntry lottery;
            SlotEntry slot;
            var lotteryFound = false;
            var slotFound = false;
            if (_context.LotteryEntry.Any(i => i.DateTime > dateTime))
            {
                lottery = _context.LotteryEntry.Where(i => i.DateTime > dateTime).OrderByDescending(i=>i.DateTime).FirstOrDefault();
                lotteryFound = true;
            } else lottery = new LotteryEntry();

            if (_context.SlotEntry.Any(i => i.DateTime > dateTime))
            {
                slot=_context.SlotEntry.Where(i => i.DateTime > dateTime).OrderByDescending(i => i.DateTime).FirstOrDefault();
                slotFound = true;
            }
            else
            {
                slot = new SlotEntry();
            }
            
            await Clients.All.SendAsync("data",new  { loter=lotteryFound,lottery=new { StarNumber=lottery.StarNumber.ToString("00"), lottery.DateTime,lottery.Date, lottery.Id } ,slot, slotFound,dateTime,DateTime.Now });
        }
    }
}
