﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Areas.Admin.Controllers
{
    [Area("Admin"),Authorize()]
    public class LotteryEntriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LotteryEntriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/LotteryEntries
        public async Task<IActionResult> Index(int page=0,int size=200)
        {
            ViewBag.page = page;   
            return View(await _context.LotteryEntry.OrderByDescending(i=>i.DateTime).Skip(page*size).Take(size).ToListAsync());
        }

        // GET: Admin/LotteryEntries/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotteryEntry = await _context.LotteryEntry
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lotteryEntry == null)
            {
                return NotFound();
            }

            return View(lotteryEntry);
        }

        // GET: Admin/LotteryEntries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/LotteryEntries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,StarNumber,Day,Month,Year,Hour,Min,AM")] LotteryEntry lotteryEntry)
        {
            var hour = lotteryEntry.AM == 0 ? lotteryEntry.Hour : (lotteryEntry.Hour + 12);
            var slots = new DateTime(lotteryEntry.Year, lotteryEntry.Month, lotteryEntry.Day, hour, lotteryEntry.Min, 0); //TimeZoneInfo.ConvertTime(new DateTime(lotteryEntry.Year, lotteryEntry.Month, lotteryEntry.Day, hour, lotteryEntry.Min, 0), TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            lotteryEntry.DateTime = slots;
            if (ModelState.IsValid)
            {
                _context.Add(lotteryEntry);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lotteryEntry);
        }

        // GET: Admin/LotteryEntries/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotteryEntry = await _context.LotteryEntry.FindAsync(id);
            if (lotteryEntry == null)
            {
                return NotFound();
            }
            return View(lotteryEntry);
        }

        // POST: Admin/LotteryEntries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,DateTime,StarNumber")] LotteryEntry lotteryEntry)
        {
            if (id != lotteryEntry.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lotteryEntry);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LotteryEntryExists(lotteryEntry.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lotteryEntry);
        }

        // GET: Admin/LotteryEntries/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lotteryEntry = await _context.LotteryEntry
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lotteryEntry == null)
            {
                return NotFound();
            }

            return View(lotteryEntry);
        }

        // POST: Admin/LotteryEntries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var lotteryEntry = await _context.LotteryEntry.FindAsync(id);
            _context.LotteryEntry.Remove(lotteryEntry);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LotteryEntryExists(string id)
        {
            return _context.LotteryEntry.Any(e => e.Id == id);
        }
    }
}
