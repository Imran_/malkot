﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class SlotEntryViewModel
    {
        public DateTime Date { get; set; }
        public string[] Ga { get; set; }
        public string[] Ra { get; set; }
        public string[] Su { get; set; }
        public string[] St { get; set; }
    }

    public class LoterryVm
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int HH { get; set; }
        public int MM { get; set; }
        [EnumDataType(typeof(AMPM))]
        public  AMPM AmPm { get; set; }
        public string GoaLucky { get; set; }
        public int NextDrawMinutes { get; set; }
    }
    public enum AMPM
    {
        AM,PM
    }
}
