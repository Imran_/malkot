﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class LotteryEntry
    {
        [NotMapped]
        public int Day { get; set; }
        [NotMapped]
        public int Month { get; set; }
        [NotMapped]
        public int Year { get; set; }
        [NotMapped]
        public int Hour { get; set; }
        [NotMapped]
        public int Min { get; set; }
        [NotMapped]
        public int AM { get; set; }

        public string Id { get; set; }
        public DateTime DateTime { get; set; }

        public int StarNumber { get; set; }

        public string Date => DateTime.Date.ToString("dd/MM/yyyy");

        public string Time => DateTime.ToString("hh:mm tt");
        
    }
}
