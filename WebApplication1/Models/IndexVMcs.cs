﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class IndexVMcs
    {
        public IndexVMcs(DateTime? date)
        {
            Date = date;
            LotteryEntries = new List<LotteryEntry>();
            SlotEntries = new List<SlotEntry>();
        }

        public DateTime? Date { get; set; }
        public IList<LotteryEntry> LotteryEntries { get; set; }
        public IList<SlotEntry> SlotEntries { get; set; }

        public DateTime SelectedDate => Date.HasValue ? Date.Value : DateTime.Now;
    }
}
