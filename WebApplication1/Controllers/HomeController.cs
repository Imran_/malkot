﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index(DateTime? date)
        {
            var vm = new IndexVMcs(date);
            vm.LotteryEntries = _context.LotteryEntry.Where(i => i.DateTime.Date == vm.SelectedDate.Date).OrderByDescending(i=>i.DateTime).ToList();
            vm.SlotEntries = _context.SlotEntry.Where(i => i.DateTime.Date == vm.SelectedDate).OrderByDescending(i=>i.DateTime).ToList();

            return View(vm);
        }

        public IActionResult Buy()
        {
            

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //public async Task<string> Result(DateTime? date)
        //{
        //    if (date == null)
        //    {
        //        date = DateTime.Now;
        //    } 
        //    using(var httpClient = new HttpClient())
        //    {

        //        var keyVal = new List<KeyValuePair<string, string>>() {
        //            new KeyValuePair<string, string>("selectDate",date.Value.ToString("dd/MM/yyyy")) 
        //        };
        //        httpClient.set
        //        HttpContent Content = new FormUrlEncodedContent(keyVal);
        //        var result = await httpClient.PostAsync("http://www.playshrigoagems.com/results/showResults", Content);
        //         var retu = await result.Content.ReadAsStringAsync();
        //        return retu;
        //    }
        //}
    }
}
