﻿"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/drawHub").build();

//Disable send button until connection is established
User = [];

connection.on("data", function (data) {
    if (!data.loter) {
        document.getElementById("errorLG").innerHTML = "No Draw"; 
        document.getElementById("lg").innerHTML = "--";
    } else {
        document.getElementById("lg").innerHTML = data.lottery.starNumber;
    }
    if (!data.slotFound) {
        document.getElementById("errorGA").innerHTML = "No Draw";
        document.getElementById("errorGB").innerHTML = "No Draw";
        document.getElementById("errorGC").innerHTML = "No Draw";
        document.getElementById("ga").innerHTML ="--";
        document.getElementById("gc").innerHTML = "--";
        document.getElementById("gb").innerHTML = "--";
    } else {
        document.getElementById("ga").innerHTML = data.slot.goldNumber;
        document.getElementById("gc").innerHTML = data.slot.rajashriNumber;
        document.getElementById("gb").innerHTML = data.slot.shubhlakshamiNumber;
    }

    console.log(data);
});

connection.start().then(function () {
    connection.invoke("GetData").catch(function (err) {
        return console.error(err.toString());
    });
    setInterval(() => {
        connection.invoke("GetData").catch(function (err) {
            return console.error(err.toString());
        });
    }, 30000);
}).catch(function (err) {
    return console.error(err.toString());
});

